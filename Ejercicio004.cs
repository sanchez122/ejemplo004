﻿using System;

namespace ConsoleApp28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ejercicio004!");

            char letra;

            for (letra = 'Z'; letra >= 'A'; letra--)
                Console.Write("{0} ", letra);
        }
    }
}
